# Proyecto Didáctico ESP32 + APDS9960 (sensor color)

## Descripción

El sensor APDS9960 tiene, entre sus prestaciones, la posibilidad de detectar la cantidad de rojo, verde y azul (RGB) de objetos (además de proximidad y gestos simples), lo que nos ofrece la posibilidad de, desde el punto de vista didáctico, utilizarlo para aplicación de conceptos como:

- PWM
- Funciones
- mapeo de valores

En el caso del proyecto montado, usando un ESP32 Dev Kit genérico, se hace necesario mapear las lecturas del sensor, que van de 0 a 4095, al valor máximo soportado por la resolución del PWM.

Tres LEDs, con los respectivos colores, ilustran la intensidad de R, G y B sensados.

En un LCD se muestran las lecturas originales.

Nota: el tipo luz ambiente modifica sustancialmente las lecturas, lo mismo que la presencia de sombras, razón por la cual se liberó al sensor de la protoboard.

## TODO / Mejoras propuestas

- Realizar experiencias de medición con diferentes tipos de iluminación (cálida, fría, del sol)
- Reconocer colores y analizar las mediciones de acuerdo a la proximidad del objeto (puede utilizarse el mismo sensor)

## Entorno de programación

PlatformIO con VS Code

## Librerías

[Librería ARDUINO_APDS9960](https://github.com/arduino-libraries/Arduino_APDS9960)

[Librería LiquidCrystal I2C](https://reference.arduino.cc/reference/en/libraries/liquidcrystal-i2c/)
