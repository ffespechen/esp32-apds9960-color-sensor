// Basado en el ejemplo APDS-9960 - Color Sensor
// De la librería https://github.com/arduino-libraries/Arduino_APDS9960

#include <Arduino.h>
#include <Arduino_APDS9960.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

const int FRECUENCIA = 5000;
const int CANAL_ROJO = 0;
const int CANAL_VERDE = 1;
const int CANAL_AZUL = 2;
const int RESOLUCION = 8;

// Lectura de color 4095 max
// PWM máx resolution 255
const int MAX_COLOR = 4095;
const int MAX_DUTY = 255;

// LEDs
const int LED_ROJO = 19;
const int LED_VERDE = 18;
const int  LED_AZUL = 17;


void setup() {
  Serial.begin(115200);
  while (!Serial);

  // Inicializar el LCD
  lcd.init();
  lcd.backlight();


  // Inicializar los LEDs para PWM
  ledcSetup(CANAL_ROJO, FRECUENCIA, RESOLUCION);
  ledcAttachPin(LED_ROJO, CANAL_ROJO);

  ledcSetup(CANAL_VERDE, FRECUENCIA, RESOLUCION);
  ledcAttachPin(LED_VERDE, CANAL_VERDE);

  ledcSetup(CANAL_AZUL, FRECUENCIA, RESOLUCION);
  ledcAttachPin(LED_AZUL, CANAL_AZUL);

  if (!APDS.begin()) {
    Serial.println("Error initializing APDS-9960 sensor.");
  }
}

void mostrarComposicionColores(int r, int g, int b){
  int rojo = map(r, 0, MAX_COLOR, 0, MAX_DUTY);
  int verde = map(g, 0, MAX_COLOR, 0, MAX_DUTY);
  int azul = map(b, 0, MAX_COLOR, 0, MAX_DUTY);

  ledcWrite(CANAL_ROJO, rojo);
  ledcWrite(CANAL_VERDE, verde);
  ledcWrite(CANAL_AZUL, azul);

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("APDS9960 - Colores");
  lcd.setCursor(0, 1);
  lcd.print("r");
  lcd.setCursor(5, 1);
  lcd.print("g");
  lcd.setCursor(10, 1);
  lcd.print("b");

  lcd.setCursor(1, 1);
  lcd.print(r);
  lcd.setCursor(6, 1);
  lcd.print(g);
  lcd.setCursor(11, 1);
  lcd.print(b);
}

void loop() {
  // Verificar si está disponible la lectura de colores
  while (! APDS.colorAvailable()) {
    delay(5);
  }
  int r, g, b;

  // Leer los colores
  APDS.readColor(r, g, b);

  // Mostrar los valores en LCD y en LEDs
  mostrarComposicionColores(r, g, b);

  delay(1000);
}